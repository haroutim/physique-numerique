function dw = VX(f,r)
  Rmax = 10 ; %Rayon maximal de la tornade
  Vmax = 30 ; %Vitesse maximal
  nu=15.6*10^(-6); %Viscosit� cin�matique de l'air
  a=4*nu/(Rmax^2); 
  dw(1)=f(2);
  dw(2)=-(1/r+a*r/(2*nu))*f(2)-a/nu*f(1);
  dw(3)=-f(3)/r+2*f(1);
endfunction
