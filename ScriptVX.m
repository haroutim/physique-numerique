L=50; %Longueur
N=150; %Nombre de point
nu=15.6*10^(-6); %Viscosit� cin�matique 
nb=50; 
%% Coordonn� cylindrique
r=linspace(0.0001,L,N)'; %Param�tre r rayon 
theta=linspace(0,2*pi,N)';%Param�tre theta 
z=linspace(0,L,N)';%Param�tre z altitude
Rmax = 10 ;
Vmax = 30 ;
a=4*nu/(Rmax^2);
%% Condition initial
omega0=Vmax/Rmax;
omegaPrime=0;
V0=0;
f0=[omega0;omegaPrime;V0];
%% Int�gration num�rique
Z=lsode(@(f,r)vortex(f,r),f0,r);
%% Vitesse Cylindrique
vtheta=Z(:,3);
vr =-a*r/2;
vz =a*z;
%% Vitesse cartesienne
vx=vr.*cos(theta)-vtheta.*sin(theta);
vy=vr.*sin(theta)+vtheta.*cos(theta);
%% Coordonn� cart�sienne
x=r.*cos(theta);
y=r.*sin(theta);
%% Solution th�orique
omegaTH=omega0*exp(-a/4/nu.*r.^2); %vorticit�
vTH_theta=4*nu*omega0/a./r.*(1-exp(-a/4/nu*r.^2)); %Vitesse orthoradiale
plot(r,vtheta,r,vTH_theta)
title('Comparaison num�rique/analytique')
legend('num�rique','analytique')
xlabel('r(m)')
ylabel('v(m/s)')
%% Vitesse plan tornade
function res = etoile2D(L,nb,Ntheta,vtheta)
  b=L;
  
  xmin=0;
  ymin=0;
  xmax=b;
  ymax=b;
  
  x1=linspace(xmin,xmax,nb)';
  y1=linspace(ymin,ymax,nb)';
  
  r=(x1.^2+y1.^2).^0.5;
  theta=[0:2*pi/Ntheta:2*pi];
  n=0;
  X=zeros(size(r)(1),size(theta)(1));
  Y=X;
  U=zeros(size(vtheta)(1),size(theta)(1));
  V=U;
  %imagesc(x1,y1,[vx,vy])
  
  for i=1:Ntheta
    X(:,i)=r(:,1).*cos(theta(i));
    Y(:,i)=r(:,1).*sin(theta(i));
    U(:,i)=-vtheta(:,1).*sin(theta(i));
    V(:,i)=vtheta(:,1).*cos(theta(i));
    n=n+1;
  endfor
  q = quiver(X,Y,U,V);
  res=q;
endfunction
etoile2D(L,N,8,vtheta)
title('Vitesse plan tornade(theta)')
xlabel('r(m)')
ylabel('r(m)')
legend('vtheta')
%% Champs (scalaire) de vitesse 


tx=linspace(-L,L,N)'; 
ty=tx;
tz=linspace(0,L,N)';



hold('off')
[xx,yy,zz]=meshgrid(tx,ty,tz);
rr=sqrt(xx.^2+yy.^2);
V=-a*rr/2+4*nu*omega0/a./rr.*(1-exp(-a/4/nu*rr.^2))+a*zz; %Vitesse Th�orique (car valid�e)
slice(xx,yy,zz,V,[],[],0) %plan XY
hold('on')
slice(xx,yy,zz,V,0,[],[]) %plan YZ
hold('on')
slice(xx,yy,zz,V,[],0,[]) %plan XZ
colorbar
hold('off')
xlabel('m')
ylabel('m')
zlabel('m')
title('Champ (scalaire) de la vitesse' )
legend('vitesse en m/s')
