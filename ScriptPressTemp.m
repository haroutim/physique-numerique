%%A la recherche de la pression et de la temp�rature: 


N=150; %nombre de point
Dr=50; %1/2 distance
h=100; %hauteur max
tx=linspace(-Dr,Dr,N)';
ty=tx;
tz=linspace(0,h,N)';
tr=sqrt(tx.^2+ty.^2);
[XX,YY,ZZ]=meshgrid(tx,ty,tz);%matrice d'espace
RR=sqrt(XX.^2+YY.^2);
nu=2*10^(-6);%viscosit�
g=9.81;%pesenteur
Rmax = 10;
Vmax = 30 ;
a=4*nu/(Rmax^2);
omega0=Vmax/Rmax;%Condition initial de la vorticit�
rho = 1,204;%masse volumique a 20�C
Vtheta=4*omega0*nu/a*(1-exp(-a*RR.^2/4/nu))./RR;%Vitesse th�orique orthoradiale 

%%grad(p)=-rho.(v.grad)v+rho.nu.Laplacien(v)

dpr=rho*(a^2*RR/2+Vtheta.^2./RR);
dptheta=0;
dpz=-rho*a^2*ZZ-rho*g;

dp = dpr+dptheta+dpz;
%%
function F = primtiveR(f,F0,t)
  N=size(f)(1);
  dt=diff(t)(2); %differentiel
  DF=zeros(size(f));
  for i=1:N
    for j=1:N
      dF(i,j,:)=f(i,j,:).*dt; %�quivaut a dF = f*dt
      DF(i,j,:)=DF(i,j,:)+dF(i,j,:); % DF=int(dF)
    endfor
  endfor
  F = DF ;
endfunction
function F = primtiveZ(f,F0,t)
  N=size(f)(1);
  dt=diff(t)(2); %differentiel
  DF=zeros(size(f));
  for i=1:N
    dF(:,:,i)=f(:,:,i).*dt; %�quivaut a dF = f*dt
    DF(:,:,i)=DF(:,:,i)+dF(:,:,i); % DF=int(dF)
  endfor
  F = DF ;
endfunction
P0=101325; %pression initial a temp�rature ambiant au niveau de la mer
pr=primtiveR(dpr,P0,tr);
pz=primtiveZ(dpz,P0,tz);


%%Champ scalaire du grad(p)
slice(XX,YY,ZZ,dp ,0 ,[], [])
hold('on')
slice(XX,YY,ZZ,dp,[] , 0, [])
%hold('on')
slice(XX,YY,ZZ,dp, [] , [], 0)
hold('off')
xlabel('m')
ylabel('m')
zlabel('m')
colorbar
legend('dp en Pa')
title('champ scalaire du Gradient de pression � partir du champ de vitesses')

%%Champ(vectoriel) du grad(p) (ATTENTIEN : METTRE PEU DE POINT SINON IL CRASH)
P=P0.*ones(size(Vtheta))+pr+pz;
[dPx,dPy,dPz]=gradient(P);
quiver3(tx, ty, tz, dPx, dPy, dPz)
xlabel('m')
ylabel('m')
zlabel('m')
title('Gradient de pression � partir du champ de pression')

%% Champs de pression total
slice(XX,YY,ZZ,P ,0 ,[], [])
hold('on')
slice(XX,YY,ZZ,P,[] , 0, [])
slice(XX,YY,ZZ,P, [] , [], 0)
hold('off')
xlabel('m')
ylabel('m')
zlabel('m')
title('Champ de pression � partir du champ de vitesses')
colorbar
legend('Pression en Pa')


%%densit� de force subit au sol par la tornade : contrainte engendr� et force

surf(XX(:,:,1),YY(:,:,1),dpr(:,:,1)) %f= grad(p)
xlabel('m')
ylabel('m')
zlabel('N/m�')
title('densit� de force engendr� � partir du champ de pression au niveau du sol')
colorbar
legend('Force en N/m�')

%% force subit au sol par la tornade : contrainte engendr� et force

function F = ForceR(f,F0,T,t)
  N=size(f)(1);
  dt=diff(t)(2);%differentiel
  DF=zeros(size(f));
  for i=1:N
    for j=1:N
      dF(i,j,:)=f(i,j,1).*T(i,j).*dt*2*pi; % �quivaut a dF = f*2pi*tdt
      DF(i,j,:)=DF(i,j,:)+dF(i,j,:); % DF =int(dF)
    endfor
  endfor
  F = DF +F0 ;
endfunction
FR=ForceR(pr,0,RR(:,:,1),tr); %df= p.dS
surf(XX(:,:,1),YY(:,:,1),FR(:,:,1))
xlabel('m')
ylabel('m')
zlabel('N')
title('Force engendr� � partir du champ de pression au niveau du sol')
colorbar
legend('Force en N')





%% Champs de temp�rature : GP a sec , loi de Laplace

GMA = 1.4; %coef gamma de laplace
M = 28,976/1000; %masse molaire kg/mol
R = 8.31; %constente des gaz parfait J/mol/kg
T_infty=20; %temperature loin de la tornade �C
T0=T_infty+273.15; %�K
T = T0.*(P./P0).^(1./GMA-1)-273.15;

slice(XX,YY,ZZ,T ,0 ,[], [])
hold('on')
slice(XX,YY,ZZ,T,[] , 0, [])
%hold('on')
slice(XX,YY,ZZ,T, [] , [], 0)
hold('off')
xlabel('m')
ylabel('m')
zlabel('m')
title('Champ de temp�rature : Gaz Parfait sec loi de Laplace')
colorbar
legend('Temp�rature en �C')


%% Champs de temp�rature : GP humide

rho = 1,204	; %masse volumique a 20�C a P0
%coefficient de la formule de correction
alpha = 287.06; 
K = 273.15;
phi = 0.75; %taux d'humidit�
beta = 230.617;
gamma = 17.5043;
OMG = 241.2;

Th=(P-beta.*phi)/(rho.*alpha+beta.*alpha.*gamma./OMG); 

slice(XX,YY,ZZ,Th ,0 ,[], [])
hold('on')
slice(XX,YY,ZZ,Th,[] , 0, [])
%hold('on')
slice(XX,YY,ZZ,Th, [] , [], 0)
colorbar
xlabel('m')
ylabel('m')
zlabel('m')
title('Champ de temp�rature : Gaz Parfait humide(75%), avec formule de Magnus')
legend('Temp�rature en �C')

hold('off')
